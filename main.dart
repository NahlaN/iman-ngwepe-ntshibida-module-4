import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'signup.dart';
import 'home.dart';
import 'login.dart';
import 'dashboard.dart';
import 'profile.dart';
import 'feature1.dart';
import 'splash.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Give Or Take',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            brightness: Brightness.dark,
            primaryColor: Colors.black,

            fontFamily: 'Roboto',

            textTheme: const TextTheme(
              headline1: TextStyle(
                color: Colors.green, fontSize: 33.0, fontFamily: 'Open Sans', fontWeight: FontWeight.bold),
              headline2: TextStyle(
                color: Colors.green, fontSize: 20.0, fontFamily: 'Roboto Slab'),
              headline3: TextStyle(
                color: Colors.tealAccent, fontSize: 28.0, fontFamily: 'Open Sans', fontWeight: FontWeight.bold),
              headline4: TextStyle(
                      fontSize: 19, color: Colors.black, fontWeight: FontWeight.bold),
              bodyText1: TextStyle(
               color: Colors.green, fontSize: 18.0, fontFamily: 'Montserrat', letterSpacing: 2.5,
                fontWeight: FontWeight.bold),
              bodyText2: TextStyle(
                color: Colors.green,  fontSize: 12.0, fontFamily: 'Futura PT'),
            )),
        initialRoute: '/',
        routes: {
          '/': (context) => const SplashScreen(),
          '/one': (context) => const HomePage(),
          '/two': (context) => const Signup(),
          '/three': (context) => const Login(),
          '/four': (context) => const Dashboard(),
          '/five': (context) => const Profile(),
          '/last': (context) => const Feature1(),
        });
  }
}
